#include "Email.h"

Email::Email() {}

Email::Email(const string &Sender, const list <string> &Recipients, time_t DateTime, const string &Subject,
             const string &Body, list <Attachment> attachments_vect) : Sender(Sender), Recipients(Recipients),
                                                                        DateTime(DateTime), Subject(Subject),
                                                                        Body(Body),
                                                                        attachments_list(attachments_vect) {}

const string &Email::getSender() const {
    return Sender;
}

const list <string> &Email::getRecipients() const {
    return Recipients;
}

time_t Email::getDateTime() const {
    return DateTime;
}

const string &Email::getSubject() const {
    return Subject;
}

const string &Email::getBody() const {
    return Body;
}

list <Attachment> Email::getAttachments_vect() const {
    return attachments_list;
}

void Email::setSender(const string &Sender) {
    string temp = Sender;
    bool valid = std::regex_match(temp, std::regex("(\\w+)(\\.|_)?(\\w*)@(\\w+)(\\.(\\w+))+"));
    if(valid){
        this->Sender = Sender;
    } else{
        while(!std::regex_match(temp, std::regex("(\\w+)(\\.|_)?(\\w*)@(\\w+)(\\.(\\w+))+"))){
            cout << "Sender does not match pattern, enter again!" << endl;
            cin >> temp;
        }
        this->Sender = temp;
    }
}

void Email::setRecipients(const list <string> &Recipients) {
    Email::Recipients = Recipients;
}

void Email::setDateTime(time_t DateTime) {
    Email::DateTime = DateTime;
}

void Email::setSubject(const string &Subject) {
    Email::Subject = Subject;
}

void Email::setBody(const string &Body) {
    int minSize = 1;
    if (Body.length() >= minSize) {
        cout << "Body length must be greater than 1 charachter\n" << endl;
    }
    Email::Body = Body;
}

void Email::setAttachments_vect(list <Attachment> attachments_vect)  {
    Email::attachments_list = attachments_vect;
}

ostream &operator<<(ostream &os, const Email &email) {
    string recipientString = "";
    bool opening = 1;
    for (list<string>::const_iterator i = email.Recipients.begin(); i != email.Recipients.end(); i++) {
        if (!opening) {
            recipientString += ",";
        } else {
            opening = 0;
        }
        recipientString += *i;
    }
    string attachmentString;
    opening = 1;
    for(list<Attachment>::const_iterator i = email.attachments_list.begin(); i != email.attachments_list.end(); i++){

        if (!opening) {
            attachmentString += "\n";
        } else {
//            attachmentString += email.attachments_list.size() + " ";
            opening = 0;
        }
        attachmentString += i->toString();
    }
    string subjectCopy = email.Subject;
    string bodyCopy = email.Body;
    replace(attachmentString.begin(), attachmentString.end(), ' ', '_');
    replace(subjectCopy.begin(), subjectCopy.end(), ' ', '_');
    replace(bodyCopy.begin(), bodyCopy.end(), ' ', '_');

    os << email.Sender << " " << recipientString << " " << email.DateTime
       << " " << subjectCopy << " " << bodyCopy << " " << email.attachments_list.size() << "  " << attachmentString;
    return os;
}


bool Email::operator==(const Email &rhs) const {
    return Sender == rhs.Sender &&
           Recipients == rhs.Recipients &&
           DateTime == rhs.DateTime &&
           Subject == rhs.Subject &&
           Body == rhs.Body &&
           attachments_list == rhs.attachments_list;
}

bool Email::operator!=(const Email &rhs) const {
    return !(rhs == *this);
}

void Email::printPretty() {
    cout << "Sender : " << Sender << endl;
    cout << "Reciepents : ";
    for(list<string>::const_iterator i = Recipients.begin(); i != Recipients.end();i++){
        cout << (*i)  << " ";
    }
    cout << endl;
    cout << "Subject : " << Subject << endl;
    cout << "Body : " << Body << endl;
    cout << "Attachments : " ;
    for(list<Attachment>::const_iterator i = attachments_list.begin(); i != attachments_list.end();i++){
        cout << (*i).getFileName()<<"." << (*i).getFileSuffix() << " " ;
    }
    cout << endl;

}
