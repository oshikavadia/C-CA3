//
// Created by oshi on 12/5/16.
//

#include "Utils.h"

void getFileContents(const string &path, char arr[Attachment::MAX_SIZE]) {
    ifstream in(path);
    int array_size = Attachment::MAX_SIZE;
//    char *out = new char[array_size];
    int position = 0;
    if (in) {
        if (getFileSize(path) <= array_size) {
            while (!in.eof() && position < array_size) {
                in.get(arr[position]);
                position++;
            }
            in.close();
        } else {
            cout << "File over size limit. select a file under 1mb;" << endl;
        }
    } else {

    }

}

streampos getFileSize(const string &path) {
    ifstream file(path, ios::binary | ios::ate);
    return file.tellg();
}

string getFileName(const string &path) {
    string temp;
    size_t found = path.find_last_of("/\\");
    size_t found1 = path.find_last_of(".");
    temp =  path.substr(found, found1);
    return temp;
}

string getFileExtension(const string &path) {
    string temp;
    size_t found = path.find_last_of(".");
    temp =  path.substr(found, path.length()-1);
    return temp;
}

std::vector<std::string> split(std::string data, std::string delimiter)
{
    std::vector<std::string> outVector;
    std::string strElement;
    std::size_t oldPos = -1;
    std::size_t pos = data.find(delimiter, oldPos  + 1);
    while (pos != std::string::npos)
    {
        strElement = data.substr(oldPos + 1, pos - oldPos - 1);
        outVector.push_back(strElement);
        oldPos = pos;
        pos = data.find(delimiter, oldPos+1);
    }
    return outVector;
}

//https://stackoverflow.com/questions/18972258/index-of-nth-occurrence-of-the-string
template<typename Iter>
Iter nth_occurence(Iter first, Iter last,
                   Iter first_, Iter last_,
                   unsigned nth)
{
    Iter it = std::search(first, last, first_, last_);
    if (nth == 0) return it;
    if (it == last) return it;
    return nth_occurence(it + std::distance(first_, last_), last,
                         first_, last_, nth -1);
}
