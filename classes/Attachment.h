//
// Created by oshi on 12/5/16.
//

#ifndef C_CA3_ATTACHMENT_H
#define C_CA3_ATTACHMENT_H

#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

class Attachment {
public:
    const static int MAX_SIZE = 1000000;
//    Attachment(const string &FileName, const string &FileSuffix, char *FileData);

    Attachment(const string &FileName, const string &FileSuffix, char (*FileData)[MAX_SIZE]);

    virtual ~Attachment();

    const string &getFileName() const;

    void setFileName(const string &FileName);

    const string &getFileSuffix() const;

    void setFileSuffix(const string &FileSuffix);

    const char *getFileData() const;

    bool operator==(const Attachment &rhs) const;

    bool operator!=(const Attachment &rhs) const;

    friend ostream &operator<<(ostream &os, const Attachment &attachment);

    string toString()const;

private :
    string FileName;
    string FileSuffix;
    char *FileData;
public:
    Attachment();
};
#include "Utils.h"

#endif //C_CA3_ATTACHMENT_H
