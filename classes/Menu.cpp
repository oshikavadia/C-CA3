#include "Menu.h"

string Menu::MenuHeaderBorder = " *************************** ";

Menu::~Menu() {

}

const string &Menu::getName() const {
    return name;
}

void Menu::setName(const string &name) {
    Menu::name = name;
}

int Menu::getExitIndex() const {
    return exitIndex;
}

void Menu::setExitIndex(int exitIndex) {
    Menu::exitIndex = exitIndex;
}

const list <string> &Menu::getOptionList() const {
    return optionList;
}

void Menu::setOptionList(const list <string> &optionList) {
    Menu::optionList = optionList;
}

int Menu::showMenuGetChoice(const string &strPrompt) {
    if (optionList.size() == 0) {
        return -1;
    }

    cout << endl << MenuHeaderBorder << endl;
    int index = 1;
    for (string s : optionList) {
        cout << /*"[" << index << "] "*/ s << endl;
        index++;
    }
    int choice;
    cout << "Please enter your choice: ";
    cin >> choice;
    return choice;
}

bool Menu::add(const string &s) {
    bool found = (std::find(optionList.begin(), optionList.end(), s) != optionList.end());
    if (!found) {
        return add(s);
    }
    return false;
}

bool Menu::addAll(const string sArray[], int size) {
    bool success = false;
    for (int i = 0; i < size; i++) {
        success = add(sArray[i]);
    }
    return success;
}

int Menu::size() {
    return optionList.size();
}

void Menu::clear() {
    optionList.clear();
}

Menu::Menu(const string &name, int exitIndex, const list <string> &optionList) : name(name), exitIndex(exitIndex),
                                                                                 optionList(optionList) {}
