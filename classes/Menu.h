
#ifndef C_CA3_MENU_H
#define C_CA3_MENU_H

#include <iostream>
#include <algorithm>
#include <string>
#include <list>

using namespace std;

class Menu {
private:
    string name;
    int exitIndex;
    list <string> optionList;
public:
    static string MenuHeaderBorder;

    int showMenuGetChoice(const string &strPrompt);

    bool add(const string &s);

    bool addAll(const string sArray[], int size);

    int size();

    void clear();

    Menu();

    Menu(const string &name, int exitIndex, const list <string> &optionList);

    virtual ~Menu();

    const string &getName() const;

    void setName(const string &name);

    int getExitIndex() const;

    void setExitIndex(int exitIndex);

    const list <string> &getOptionList() const;

    void setOptionList(const list <string> &optionList);
};


#endif //C_CA3_MENU_H
