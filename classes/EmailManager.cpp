//
// Created by oshi on 12/6/16.
//

#include "EmailManager.h"

EmailManager::EmailManager() {}

EmailManager::~EmailManager() {

}

void EmailManager::add(const Email &e) {
    emailList.push_back(e);
}

list <Email> EmailManager::get(const string &sender) {
    list <Email> Returnlist;

    list<Email>::iterator iter = emailList.begin();
    while (iter != emailList.end()) {
        Email temp = *(iter);
        if (temp.getSender() == sender) {
            Returnlist.push_back(temp);
            iter++;
        }

    }
    return Returnlist;
}

list <Email> EmailManager::getAll() {
    return emailList;
}

void EmailManager::deleteEmail(Email e) {
    list<Email>::iterator iter = emailList.begin();
    while (iter != emailList.end()) {
        Email temp = *(iter);
        if (temp != e) {
            iter++;
        } else {
            emailList.erase(iter);
            break;
        }

    }
}

void EmailManager::deleteAll() {
    emailList.clear();
}

list <Email> EmailManager::search(const string &subject) {
    list<Email>::iterator iter = emailList.begin();
    list <Email> returnList;
    while (iter != emailList.end()) {
        Email temp = *(iter);
        if (temp.getSubject() == subject) {
            returnList.push_back(temp);
        }
        iter++;

    }
    return returnList;
}

list <Email> EmailManager::searchText(const string text) {
    list<Email>::iterator iter = emailList.begin();
    list <Email> returnList;
    regex e (text);
    while (iter != emailList.end()) {
        Email temp = *(iter);
        if (regex_search(temp.getBody().begin(),temp.getBody().end(),e)) {
            returnList.push_back(temp);
        }
        iter++;

    }
    return returnList;
}

void EmailManager::load(const string &path) {
    ifstream in(path);
    string sender, recipients, body, subject;
    list <string> attList, rcpList;
    list <Attachment> att;
    time_t dt;
    if (in) {
        int count;
        in >> count;
        while (!in.eof()) {
//            for (int i = 0; i < count; i++) {


            in >> sender;
            in >> recipients;
            in >> dt;
            in >> subject;
            in >> body;
            int attCount;
            in >> attCount;
            string temp;
            for (int y = 0; y < attCount; y++) {
                getline(in, temp);
                attList.push_back(temp);
            }
//            }
            vector<string> spltVect = split(recipients + ",", ",");
            vector<string> attVect = split(recipients + "\n", "\n");
            for (string s : attVect) {
                /*    replace(s.begin(), s.end(), '_', ' ');
                    replace(s.begin(), s.end(), '\t', '\n');
                    string fname,ext;

                    fname = s.substr(0,s.find_first_of(' '));
    //                ext = s.substr(s.find_first_of(' '),fin)
                    string b = " ";
                    auto it1 = nth_occurence(begin(s), end(s), begin(b), end(b), 1);
                    ext = s.substr(s.find_first_of(" "),std::distance(begin(s), it1));
                    char *data =  new char(sizeof(s.substr(std::distance(begin(s), it1),s.length()-1)));
                    strncpy(data,s.substr(std::distance(begin(s), it1),s.length()-1), sizeof(s.substr(std::distance(begin(s), it1),s.length()-1)));
                    Attachment a(fname,ext,data);
                    att.push_back(a);
    */
            }
            copy(spltVect.begin(), spltVect.end(), std::back_inserter(rcpList));
            Email e(sender, rcpList, dt, subject, body, att);
            emailList.push_back(e);
        }
        in.close();
    } else {

    }
}

void EmailManager::save(const string &path) {
    ofstream out(path);
    if (out) {
        out << emailList.size() << endl;
        for (list<Email>::const_iterator i = emailList.begin(); i != emailList.end(); i++) {
            out << *i << endl;
        }
        out.close();
    } else {

    }
}

Email EmailManager::getByIndex(int i) {
    if (emailList.size() > i) {
        std::list<Email>::iterator it = emailList.begin();
        std::advance(it, i);
        return (Email) *it;
        // 'it' points to the element at index 'N'
    }
}

list <Email> EmailManager::searchByAtt() {
    list <Email> l;
    for (list<Email>::const_iterator i = emailList.begin(); i != emailList.end(); i++) {
        if ((*i).getAttachments_vect().size() > 0) {
            l.push_back((*i));
        }
    }
    return l;
}
