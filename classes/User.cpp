//
// Created by oshi on 11/29/16.
//

#include "User.h"
const string &User::getEmail() const {


    return email;
}

void User::setEmail(const string &email) {
    string temp = email;
    bool valid = std::regex_match(temp, std::regex("(\\w+)(\\.|_)?(\\w*)@(\\w+)(\\.(\\w+))+"));
    if(valid){
        this->email = email;
    } else{
        while(!std::regex_match(temp, std::regex("(\\w+)(\\.|_)?(\\w*)@(\\w+)(\\.(\\w+))+"))){
            cout << "Sender does not match pattern, enter again!" << endl;
            cin >> temp;
        }
        this->email = temp;
    }
}

const string &User::getpassword() const {
    return password;
}

void User::setpassword(const string &password) {
    //Password must be atleast 8 characters long and include a digit.
    const int maxSize = 20;
    int countDigits = 0;
    int minSize = 8;
    //const int size = password.size;
    if (password.length()< minSize) {
        cout << "Please enter a valid password length\n" << endl;
    }
    else if (password.length()>maxSize)
    {
        cout << "Your password is too long\n" << endl;
    }

    for (int i = 0; i < password.length(); i++)
    {
        if (isdigit(password[i]))
            ++countDigits;
    }
    if (countDigits != 1)
    {
        cout << "Password must contain one digit.";
    }

}

const string &User::getUserName() const {
    return userName;
}

void User::setUserName(const string &userName) {
    string temp = userName;
    int minSize = 8;
    if (userName.length()< minSize) {
        cout << "Name must be minimum 8 charachters long\n" << endl;
    }
    while (userName.find_first_of("0123456789") != -1)
    {
        cout << "No digits are allowed in name." << endl;
        cout << "Please re-enter name:" << endl;
        cin.clear();
        cin.sync();
        getline(cin, temp);
    }
}

ostream &operator<<(ostream &os, const User &user) {
    os << "email: " << user.email << " password: " << user.password << " userName: " << user.userName;
    return os;
}

User::User() {}

User::~User() {

}

bool User::operator==(const User &rhs) const {
    return email == rhs.email &&
           password == rhs.password &&
           userName == rhs.userName;
}

bool User::operator!=(const User &rhs) const {
    return !(rhs == *this);
}
